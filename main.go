package main

import (
	"fmt"
	"time"
)

func main() {
    ch := make(chan int)
    go primes(ch)
    for {
        start := time.Now()
        digit := <-ch
        elapsed := time.Since(start)
        fmt.Printf("%v -> Time: %s\n", digit, elapsed)
    }
}

func primes(ch chan int) {
    for i := 2; ; i++ {
        for j := 2; j*j <= i; j++ {
            if i%j == 0 {
                goto next
            }
        }
        ch <- i
        next:
    }
}