# Prime Number Finder in Go

This program is written in Go and is used to find prime numbers. It uses a concurrent approach with goroutines and channels to generate and output prime numbers. 

## Code Flow

The program starts by creating a channel `ch` to receive prime numbers. Then, a goroutine is started to generate prime numbers concurrently. The prime number generation is done by the `primes()` function which uses a sieve algorithm to find prime numbers. The sieve algorithm starts by initializing a boolean array of size `n` (in this case 100000) with all elements set to `true`. The algorithm then starts from 2 and iterates until it finds a prime number. For each prime number found, it sends it to the `ch` channel, and marks all its multiples as non-prime in the sieve array.

The `main()` function then receives the prime numbers from the `ch` channel and prints them to the console. It also measures the time it took to find each prime number and prints it along with the prime number.

## Methodology

The algorithm uses a sieve algorithm to find prime numbers. A sieve algorithm is a simple, ancient algorithm for finding all prime numbers up to a given limit. It iteratively marks as composite (i.e., not prime) the multiples of each prime, starting with the multiples of 2. The sieve of Eratosthenes, one of the most efficient ways to find all primes smaller than n when n is smaller than 10 million or so.

The program also uses goroutines and channels to perform the prime number generation concurrently. Goroutines are lightweight threads of execution, and channels are used to communicate between goroutines. This allows the program to generate prime numbers concurrently and output them as soon as they are found, without waiting for the entire prime number generation process to complete.

## Running the Program

To run the program, you will need to have Go installed on your machine. Once you have Go installed, you can run the program by navigating to the root directory of the project and running the following command:

```sh
go run main.go
```

This will start the program, and it will begin generating and outputting prime numbers. To stop the program, you can use the `Ctrl-C` keyboard shortcut.

## Conclusion

This program demonstrates how to use a sieve algorithm and goroutines to generate prime numbers in Go. It also shows how to use channels to communicate between goroutines and measure the time taken for finding each prime number.

It's worth noting that this program is not optimized for large prime numbers and will become very slow as the numbers get larger. If you need to find large prime numbers, you should consider using specialized libraries or tools that are optimized for this purpose.

## Contributions

- Pull requests are welcome
- If you have any suggestions or issues, please open them

## License

This code is open-sourced under the [MIT license](https://opensource.org/licenses/MIT). Please feel free to use, modify, and distribute the code as you wish.